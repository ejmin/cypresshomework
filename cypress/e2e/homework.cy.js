
// The test scenario is the following:
// The test script shall
//   ** Start **
//       - start the browser and open https://salsita-qa-homework.netlify.app/
//   ** Main Page **
//       - click the Enter button -- "code" page loads
//   ** Code Page **
//       - on the code page: find a "secret" input element and enter its value into the input field
//       - ensure that the "robot" checkbox is checked
//       - submit the form -- "lists" page loads (list of famous quotes)
//   ** Lists Page **
//       On this page there are quote categories "Famous Quotes" and "Awesome Quotes",
//       each having several quotes. Each quote has a score number in parentheses.
//       At the bottom there is a "Total score" number.
//       The texts (category names and quotes in each category) are always the same,
//       however they are presented in random order. The score numbers are volatile.
//       The test script shall:
//       - verify that all the categories and their quotes are displayed. No extra quotes, no missing ones.
//       - verify that the "Total score:" is the sum of all quote scores
      
// Notes:
// - keep it simple, test only the scenario above (not negative scenarios or other things)

describe('homework', () => {

  beforeEach(() => {
    // - start the browser and open https://salsita-qa-homework.netlify.app/
    cy.visit('https://salsita-qa-homework.netlify.app/')
    cy.viewport(1920,1080)
  })


  it('the only scenario', () => {
    // - click the Enter button -- "code" page loads
    cy.get('#root > div > a').click()
    cy.get('#root > div > h3').should('have.text', 'Please enter the secret code')

    // - on the code page: find a "secret" input element and enter its value into the input field
    cy.get('#root > div > form > div > input[type=hidden]:nth-child(8)').then(($secret) => {
      cy.get('#code').type($secret.val())
    })

    // - ensure that the "robot" checkbox is checked
    cy.get('#isRobot').check()

    // - submit the form -- "lists" page loads (list of famous quotes)
    cy.get('#root > div > form > button').click()
    
    // - verify that all the categories and their quotes are displayed. No extra quotes, no missing ones.
    cy.get('#root > div > ul > ul[qa-id=Awesome] > li').should('have.length', 5)
    cy.get('#root > div > ul > ul[qa-id=Famous] > li').should('have.length', 5)
      
    // - verify that the "Total score:" is the sum of all quote scores
    let score = 0
    cy.get('#root > div > ul > ul > li').each(($item) => {
      score += parseInt($item.find('.score').text())
    })
    cy.get('#summary').then(($value) => {
      cy.wrap(parseInt($value.text().match(/\d+/)[0])).should('eq', score)
    })

  })  
})